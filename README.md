### What is this repository for? ###
This repo. is just a demo to allow user to search for images on Flickr by using keywords.

### How do I get set up? ###

* Summary of set up:

  * Just clone it and run the application, then search for any keyword.

  * Dependencies:

  -Two libraries were included in the project

  * Picasso library to show images
  
  * compile 'com.mcxiaoke.volley:library:1.0.19'
  
  - rxjava to help with parsing data