package com.smartec;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.smartec.fragments.SearchFragment;

public class MainActivity extends AppCompatActivity
{
    protected final String TAG = getClass().getSimpleName();
    public static final String CONTENT_FRAGMENT = "CONTENT_FRAGMENT";
    protected ProgressBar mainProgressBar;
    private String themeKey = "color";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

//        SharedPreferences prefs = this.getSharedPreferences("com.smartec", Context.MODE_PRIVATE);
//        int key = prefs.getInt(themeKey, 0);
        setContentView(R.layout.activity_main);
        mainProgressBar = (ProgressBar) findViewById(R.id.activity_progressBar_mainProgress);
        addFragment(new SearchFragment(), false);
    }

    //region Add Fragment
    public void addFragment(Fragment fragment, boolean addToBackStack)
    {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.activity_mainView, fragment, CONTENT_FRAGMENT);

        if(addToBackStack)
        {
            transaction.addToBackStack(fragment.getClass().getName());
        }

        transaction.commit();
    }

    //region menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if(id == R.id.action_DefaultTheme)
        {
            SharedPreferences prefs = this.getSharedPreferences("com.smartec", Context.MODE_PRIVATE);
            prefs.edit().putInt(themeKey, 1).apply();

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        else if(id == R.id.action_OrangeTheme)
        {
            SharedPreferences prefs = this.getSharedPreferences("com.smartec", Context.MODE_PRIVATE);
            prefs.edit().putInt(themeKey, 2).apply();

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

    //region helpers

    public void showLoading(boolean value)
    {
        if(value)
        {
            mainProgressBar.setVisibility(View.VISIBLE);
        }
        else
        {
            mainProgressBar.setVisibility(View.GONE);
        }
    }

    //endregion
}
