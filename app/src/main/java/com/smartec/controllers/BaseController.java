package com.smartec.controllers;

import android.content.Context;

import rx.Observable;

/**
 * Created by M.Baraka on 04-Feb-16.
 *
 */
public abstract class BaseController
{
    //region fields

    protected String API_Search = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=d3610b62695f7040d4222d09d3ddc927&format=json&nojsoncallback=?&content_type=4";

    protected Context context;
    protected String logTAG = getClass().getSimpleName();

    //endregion

    public BaseController(Context context)
    {
        this.context = context;
    }

    //endregion

    public Context getContext()
    {
        return context;
    }

}
