package com.smartec.controllers;

import android.content.Context;

import com.smartec.models.ImageModel;
import com.smartec.utilities.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import rx.Observable;

/**
 * Created by M.Baraka on 04-Feb-16.
 *
 */
public class ImageController extends BaseController
{
    private ImageModel model;

    public ImageController(Context context, JSONObject jsonObject)
    {
        super(context);
        ImageModel imageModel = new ImageModel();
        try
        {
            imageModel.setTitle(jsonObject.getString("title"));

            imageModel.setId(jsonObject.getString("id"));
            imageModel.setFarmID(jsonObject.getInt("farm"));
            imageModel.setServerID(jsonObject.getInt("server"));
            imageModel.setSecreteID(jsonObject.getString("secret"));
            imageModel.setOwnerID(jsonObject.getString("owner"));

            setModel(imageModel);
        }
        catch(JSONException e)
        {
            Logger.e(e);
        }

    }

    //region getters & setters
    public String getModelURL()
    {
        return model.getUrl();
    }

    public String getTitle()
    {
        return model.getTitle();
    }

    protected void setModel(ImageModel model)
    {
        this.model = model;
    }

    protected String getModelID()
    {
        return model.getId();
    }

    public void setOwner(String ownerID)
    {
        model.setOwnerID(ownerID);
    }

    public String getOwnerID()
    {
        return model.getOwnerID();
    }
    //endregion

    @Override
    public boolean equals(Object o)
    {
        return o instanceof ImageController && ((ImageController) o).getModelID().equalsIgnoreCase(model.getId());
    }
}
