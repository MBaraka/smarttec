package com.smartec.controllers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smartec.utilities.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by M.Baraka  on 04-Feb-16.
 */
public class SearchController extends BaseController
{
    //region fields
    protected int page;
    protected int perPage;
    protected String query;
    protected HashMap<String, ArrayList<ImageController>> searchHashMap;

    private static SearchController sharedManager;

    protected ArrayList<ImageController> imageControllers;

    //endregion

    protected SearchController(Context context)
    {
        super(context);
        perPage = 10;
    }

    //region requesting data

    public Observable<SearchController> requestData(int index)
    {
        if(searchHashMap != null && searchHashMap.containsKey(getQuery()) && searchHashMap.get(getQuery()) != null
                && index < searchHashMap.get(getQuery()).size() - 3)
        {
            return Observable.create(new Observable.OnSubscribe<SearchController>()
            {
                @Override
                public void call(Subscriber<? super SearchController> subscriber)
                {
                    imageControllers = searchHashMap.get(getQuery());
                    subscriber.onNext(SearchController.this);
                    subscriber.onCompleted();
                }
            });
        }
        else
        {
            return requestData();
        }
    }

    protected Observable<SearchController> requestData()
    {
        return Observable.create(new Observable.OnSubscribe<SearchController>()
        {
            @Override
            public void call(final Subscriber<? super SearchController> subscriber)
            {
                RequestQueue queue = Volley.newRequestQueue(getContext());
                String url = getURL();
                Logger.i(logTAG, getURL());

                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response)
                            {
                                parseData(response);
                                page++;
                                subscriber.onNext(SearchController.this);
                                subscriber.onCompleted();
                            }
                        }, new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Logger.e(logTAG, error.getMessage());
                        if(searchHashMap != null && searchHashMap.containsKey(getQuery()))
                        {//if an error happens, and I have results for that keyword, then return those results
                            subscriber.onNext(SearchController.this);
                            subscriber.onCompleted();
                        }
                        else
                        {
                            subscriber.onError(error.getCause());
                        }
                    }
                });
                // Add the request to the RequestQueue.
                queue.add(stringRequest);
            }
        }).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
    }

    protected String getURL()
    {
        if(query != null)
        {
            return API_Search + "&text=" + query + "&page=" + page + "&per_page=" + perPage;
        }
        else
        {
            throw new RuntimeException("no query given, you must give a query to search for it");
        }
    }

    //endregion

    //region helpers

    /**
     * parsing data from response
     */
    protected void parseData(String response)
    {
        try
        {
            JSONObject responseJSON = new JSONObject(response);
            JSONObject jsonObject = responseJSON.getJSONObject("photos");
            page = jsonObject.getInt("page");
            perPage = jsonObject.getInt("perpage");

            JSONArray jsonArray = jsonObject.getJSONArray("photo");
            for(int i = 0; i < jsonArray.length(); i++)
            {
                ImageController imageController = new ImageController(getContext(), jsonArray.getJSONObject(i));
                addManager(imageController);
            }

            addToHasMap();
        }
        catch(JSONException e)
        {
            Logger.e(logTAG, "", e);
        }
    }

    protected void addManager(ImageController imageController)
    {
        if(imageControllers == null)
        {
            imageControllers = new ArrayList<>();
        }
        if(! imageControllers.contains(imageController))
        {
            imageControllers.add(imageController);
        }
    }

    protected void addToHasMap()
    {
        if(searchHashMap == null)
        {
            searchHashMap = new HashMap<>();
        }
        searchHashMap.put(getQuery(), getImageControllers());
    }
    //endregion

   //region getters setters
    public void setQuery(String query)
    {
        if(this.query!= null && query!=null  &&!this.query.equalsIgnoreCase(query))
        {
            if(imageControllers != null)
            {
                addToHasMap();
                imageControllers.clear();
            }
        }
        this.query = query;
    }

    public String getQuery()
    {
        return query;
    }

    public ArrayList<ImageController> getImageControllers()
    {
        return (ArrayList<ImageController>) imageControllers.clone();
    }
    //endregion

    //region singleton
    public static void init(Context context)
    {
        if(sharedManager == null)
        {
            sharedManager = new SearchController(context);
        }
    }

    public static SearchController getSharedManager()
    {
        return sharedManager;
    }

    //endregion
}
