package com.smartec.controllers;

import android.content.Context;

import com.smartec.utilities.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchUserImages extends SearchController
{

    public SearchUserImages(Context context)
    {
        super(context);
    }

    @Override
    protected String getURL()
    {
        if(query != null)
        {
            return API_Search + "&user_id=" + query + "&page=" + page + "&per_page " + perPage;
        }
        return super.getURL();
    }

    @Override
    protected void parseData(String response)
    {
        if(imageControllers == null)
        {
            imageControllers = new ArrayList<>();
        }

        try
        {
            JSONObject jsonObject = new JSONObject(response);

            jsonObject = jsonObject.getJSONObject("photos");

            page = jsonObject.getInt("page");
            perPage = jsonObject.getInt("perpage");

            JSONArray jsonArray = jsonObject.getJSONArray("photo");
            for(int i = 0; i < jsonArray.length(); i++)
            {
                ImageController imageController = new ImageController(getContext(), jsonArray.getJSONObject(i));

                if(! imageControllers.contains(imageController))
                {
                    imageControllers.add(imageController);
                }
            }
        }
        catch(JSONException e)
        {
            Logger.e(e);
        }
    }
}
