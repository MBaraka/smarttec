package com.smartec.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartec.CustomViews.CardView;
import com.smartec.MainActivity;
import com.smartec.R;
import com.smartec.controllers.ImageController;
import com.smartec.controllers.SearchController;
import com.smartec.controllers.SearchUserImages;
import com.smartec.fragments.UserImagesFragment;
import com.smartec.utilities.Logger;

import java.util.ArrayList;

import rx.Subscriber;

public class RecycleViewAdapter extends RecyclerView.Adapter
{
    private Context context;
    private ArrayList<ImageController> imageControllers;
    private SearchController searchController;

    public RecycleViewAdapter(Context context, SearchController searchController)
    {
        this.context = context;
        this.searchController = searchController;
        this.imageControllers = searchController.getImageControllers();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.card_view, null);
        return new CardView(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof CardView)
        {
            final ImageController tempController = imageControllers.get(position);
            ((CardView) holder).setImageController(tempController);

            if(!(searchController instanceof SearchUserImages))
            {
                View.OnClickListener onClickListener = new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        UserImagesFragment userImagesFragment = new UserImagesFragment();
                        userImagesFragment.setImageController(tempController);
                        if(context instanceof MainActivity)
                        {
                            ((MainActivity) context).addFragment(userImagesFragment, true);
                        }
                    }
                };
                ((CardView) holder).setOnClickListener(onClickListener);
            }
        }

        if(position > imageControllers.size() -2)
        {
            requestMoreData(position);
        }

    }

    private void requestMoreData(int index)
    {
        if(context instanceof MainActivity)
        {
            ((MainActivity) context).showLoading(true);
        }
        searchController.requestData(index).subscribe(new Subscriber<SearchController>() {
            @Override
            public void onCompleted()
            {
                if(context instanceof MainActivity)
                {
                    ((MainActivity) context).showLoading(true);
                }
            }

            @Override
            public void onError(Throwable e)
            {
                Logger.e(e);
                if(context instanceof MainActivity)
                {
                    ((MainActivity) context).showLoading(true);
                }
            }

            @Override
            public void onNext(SearchController searchController)
            {
                imageControllers = searchController.getImageControllers();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount()
    {
        if(imageControllers != null)
            return imageControllers.size();
        else
            return 0;
    }


}
