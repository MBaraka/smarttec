package com.smartec.models;

/**
 * Created by M.Baraka on 04-Feb-16.
 *
 */
public class ImageModel
{
    //region fields
    private String id, title;

    private String ownerID;

    private int serverID, farmID;
    private String secreteID;
    //endregion

    //region getters & setters
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getUrl()
    {//https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_size.jpg
        //TODO get size
        String size = null;
        return "https://farm" + farmID + ".staticflickr.com/" + serverID + "/" + id + "_" + secreteID + ".jpg";
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    protected int getServerID()
    {
        return serverID;
    }

    public void setServerID(int serverID)
    {
        this.serverID = serverID;
    }

    protected int getFarmID()
    {
        return farmID;
    }

    public void setFarmID(int farmID)
    {
        this.farmID = farmID;
    }

    protected String getSecreteID()
    {
        return secreteID;
    }

    public void setSecreteID(String secreteID)
    {
        this.secreteID = secreteID;
    }

    public String getOwnerID()
    {
        return ownerID;
    }

    public void setOwnerID(String ownerID)
    {
        this.ownerID = ownerID;
    }

    //endregion
}
