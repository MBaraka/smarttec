package com.smartec.CustomViews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.smartec.R;

public class ProgressDrawable extends AnimationDrawable
{
    private static ProgressDrawable getWhiteProgressDrawable(Context context)
    {
        ProgressDrawable progressDrawable = new ProgressDrawable();

        progressDrawable.addFrame(context, R.drawable.ic_loading_white_0);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_1);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_2);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_3);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_4);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_5);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_6);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_7);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_8);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_9);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_10);
        progressDrawable.addFrame(context, R.drawable.ic_loading_white_11);

        return progressDrawable;
    }

    private static ProgressDrawable getPrimaryBlackProgressDrawable(Context context)
    {
        ProgressDrawable progressDrawable = new ProgressDrawable();

        progressDrawable.addFrame(context, R.drawable.ic_loading_0);
        progressDrawable.addFrame(context, R.drawable.ic_loading_1);
        progressDrawable.addFrame(context, R.drawable.ic_loading_2);
        progressDrawable.addFrame(context, R.drawable.ic_loading_3);
        progressDrawable.addFrame(context, R.drawable.ic_loading_4);
        progressDrawable.addFrame(context, R.drawable.ic_loading_5);
        progressDrawable.addFrame(context, R.drawable.ic_loading_6);
        progressDrawable.addFrame(context, R.drawable.ic_loading_7);
        progressDrawable.addFrame(context, R.drawable.ic_loading_8);
        progressDrawable.addFrame(context, R.drawable.ic_loading_9);
        progressDrawable.addFrame(context, R.drawable.ic_loading_10);
        progressDrawable.addFrame(context, R.drawable.ic_loading_11);

        return progressDrawable;
    }

    private static ProgressDrawable getPrimaryProgressDrawable(Context context)
    {
        ProgressDrawable progressDrawable = new ProgressDrawable();

        progressDrawable.addFrame(context, R.drawable.ic_loading_0);
        progressDrawable.addFrame(context, R.drawable.ic_loading_1);
        progressDrawable.addFrame(context, R.drawable.ic_loading_2);
        progressDrawable.addFrame(context, R.drawable.ic_loading_3);
        progressDrawable.addFrame(context, R.drawable.ic_loading_4);
        progressDrawable.addFrame(context, R.drawable.ic_loading_5);
        progressDrawable.addFrame(context, R.drawable.ic_loading_6);
        progressDrawable.addFrame(context, R.drawable.ic_loading_7);
        progressDrawable.addFrame(context, R.drawable.ic_loading_8);
        progressDrawable.addFrame(context, R.drawable.ic_loading_9);
        progressDrawable.addFrame(context, R.drawable.ic_loading_10);
        progressDrawable.addFrame(context, R.drawable.ic_loading_11);

        return progressDrawable;
    }

    public static ProgressDrawable getDrawable(@NonNull Context context, @Nullable Style style)
    {
        if (style == null) style = Style.LargePrimary;

        switch (style)
        {
            case LargePrimaryBlackBackground:
                return getPrimaryBlackProgressDrawable(context);
            case SmallWhite:
                return getWhiteProgressDrawable(context);
            default:
                return getPrimaryProgressDrawable(context);
        }
    }

    public ProgressDrawable()
    {

    }

    private void addFrame(Context context, int resId)
    {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeResource(context.getResources(), resId));
        addFrame(bitmapDrawable, 100);
    }

    private static class BitmapDrawable extends Drawable
    {
        private static final int DEFAULT_PAINT_FLAGS =
                Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG;
        private final Bitmap bitmap;
        private final Paint paint;

        public BitmapDrawable(Bitmap bitmap)
        {
            this.bitmap = bitmap;
            paint = new Paint(DEFAULT_PAINT_FLAGS);
        }

        @Override
        public void draw(Canvas canvas)
        {
            float left = (canvas.getWidth() - bitmap.getWidth()) / 2;
            float top = (canvas.getHeight() - bitmap.getHeight()) / 2;
            canvas.drawBitmap(bitmap, left, top, paint);
        }

        @Override
        public void setAlpha(int alpha)
        {

        }

        @Override
        public void setColorFilter(ColorFilter cf)
        {

        }

        @Override
        public int getOpacity()
        {
            return PixelFormat.TRANSPARENT;
        }
    }

    public enum Style
    {
        /**
         * Large, white background, and purple foreground.
         */
        LargePrimary,
        /**
         * Large, black background, and purple foreground.
         */
        LargePrimaryBlackBackground,
        /**
         * Small, white foreground, and purple background.
         */
        SmallWhite,
    }
}
