package com.smartec.CustomViews;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.smartec.R;
import com.smartec.controllers.ImageController;
import com.smartec.utilities.Logger;
import com.squareup.picasso.Picasso;

public class CardView extends RecyclerView.ViewHolder
{
    protected ImageView imageView;
    protected TextView textView;
    private ImageController imageController;

    public CardView(View itemView)
    {
        super(itemView);
        this.imageView = (ImageView) itemView.findViewById(R.id.cardView_Image);
        this.textView = (TextView) itemView.findViewById(R.id.cardView_Title);

    }

    public void setImageController(ImageController imageController)
    {
        this.imageController = imageController;

        Drawable drawable = ProgressDrawable.getDrawable(imageController.getContext(), ProgressDrawable.Style.LargePrimary);
        Picasso.with(imageController.getContext()).load(imageController.getModelURL()).placeholder(drawable).into(imageView);
        textView.setText(imageController.getTitle());
    }


    public void setOnClickListener(View.OnClickListener onClickListener)
    {
        itemView.setOnClickListener(onClickListener);
    }
}
