package com.smartec.utilities;

import android.util.Log;

public class Logger
{
    private static boolean allowLog = true;

    public static void i(String tag, String message)
    {
        if(tag == null || message == null)
        {
            return;
        }
        if(allowLog)
        {
            Log.i(tag, message);
        }
    }

    public static void i(String message)
    {
        i(">>>>>>>>>>>>", message);
    }

    public static void e(String tag, String message)
    {
        if(tag == null || message == null)
        {
            return;
        }
        if(allowLog)
        {
            Log.e(tag, message);
        }
    }

    public static void e(String tag, String message, Exception e)
    {
        e(tag, message);
        if(allowLog)
        {
            e.printStackTrace();
        }
    }

    public static void d(String tag, String message)
    {
        if(tag == null || message == null)
        {
            return;
        }
        if(allowLog)
        {
            Log.d(tag, message);
        }
    }

    public static void e(Throwable throwable)
    {
        if(throwable != null && allowLog)
            throwable.printStackTrace();
    }

    public static boolean getAllowLog()
    {
        return allowLog;
    }
}
