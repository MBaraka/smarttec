package com.smartec.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.smartec.MainActivity;
import com.smartec.R;
import com.smartec.adapters.RecycleViewAdapter;
import com.smartec.controllers.SearchController;
import com.smartec.utilities.Logger;

import rx.Observable;
import rx.Subscriber;
import rx.android.view.ViewObservable;

public class SearchFragment extends Fragment
{
    //region fields
    private EditText edit_searchText;
    protected RecyclerView recyclerView;
    protected RecycleViewAdapter recycleViewAdapter;
    protected SearchController searchController;
    protected String logTag = getClass().getSimpleName();

    //endregion

    //region life cycles
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        View mainView = inflater.inflate(R.layout.search_fragment, null);

        edit_searchText = (EditText) mainView.findViewById(R.id.searchFragment_EditText_Search);
        textChanges(edit_searchText).distinctUntilChanged().subscribe(new Subscriber<String>()
        {
            @Override
            public void onCompleted()
            {

            }

            @Override
            public void onError(Throwable e)
            {
                Logger.e(e);
            }

            @Override
            public void onNext(String text)
            {
                search(text);
            }
        });
        edit_searchText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if(actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    search(edit_searchText.getText().toString());
                    return true;
                }
                return false;
            }
        });

        edit_searchText.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP)
                {
                    if(event.getRawX() >= (edit_searchText.getRight() - edit_searchText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width()))
                    {
                        search(edit_searchText.getText().toString());
                        return true;
                    }
                }
                return false;
            }
        });

        recyclerView = (RecyclerView) mainView.findViewById(R.id.searchFragment_RecyclerView);

        return mainView;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        getActivity().setTitle(R.string.SearchFragment);
        if(recycleViewAdapter != null)
        {
            recyclerView.setAdapter(recycleViewAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }
    }

    //endregion

    //region helpers
    protected void setSearchVisibility(boolean value)
    {
        if(value)
        {
            edit_searchText.setVisibility(View.VISIBLE);
        }
        else
        {
            edit_searchText.setVisibility(View.GONE);
        }
    }

    public Observable<String> textChanges(final TextView textView)
    {
        Observable<String> textChanges = Observable.create(new Observable.OnSubscribe<String>()
        {
            @Override
            public void call(final Subscriber<? super String> subscriber)
            {
                textView.addTextChangedListener(new TextWatcher()
                {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after)
                    {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count)
                    {

                    }

                    @Override
                    public void afterTextChanged(Editable s)
                    {
                        subscriber.onNext(s.toString());
                    }
                });
            }
        });

        return ViewObservable.bindView(textView, textChanges);
    }


    protected void showLoading(boolean value)
    {
        if(getActivity() instanceof MainActivity)
        {
            ((MainActivity) getActivity()).showLoading(value);
        }
    }
    //endregion

    //region search methods
    protected void search(String text)
    {
        if(text == null || text.equalsIgnoreCase(""))
        {
            Toast.makeText(getActivity(), "Please Enter your search keyword", Toast.LENGTH_SHORT).show();
            return;
        }
        getSearchController().setQuery(text);
        showLoading(true);
        getSearchController().requestData(0).subscribe(new Subscriber<SearchController>()
        {
            @Override
            public void onCompleted()
            {
                showLoading(false);
            }

            @Override
            public void onError(Throwable e)
            {
                if(getActivity() != null)
                {
                    Toast.makeText(getActivity(), logTag + "Error requesting images", Toast.LENGTH_SHORT).show();
                }
                showLoading(false);
                Logger.e(e);
            }

            @Override
            public void onNext(SearchController searchController)
            {
                recycleViewAdapter = new RecycleViewAdapter(getContext(), searchController);
                recyclerView.setAdapter(recycleViewAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

            }
        });
    }

    protected SearchController getSearchController()
    {
        searchController = SearchController.getSharedManager();
        if(searchController == null)
        {
            SearchController.init(getActivity());
        }
        return searchController = SearchController.getSharedManager();
    }
    //endregion
}
