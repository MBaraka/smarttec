package com.smartec.fragments;

import com.smartec.R;
import com.smartec.controllers.ImageController;
import com.smartec.controllers.SearchController;
import com.smartec.controllers.SearchUserImages;

/**
 * Created by M.Baraka on 04-Feb-16.
 *
 */
public class UserImagesFragment extends SearchFragment
{
    private ImageController imageController;

    //region helpers
    public void setImageController(ImageController imageController)
    {
        this.imageController = imageController;
    }

    @Override
    protected SearchController getSearchController()
    {
        if(searchController == null || (!(searchController instanceof SearchUserImages)))
        {
            searchController = new SearchUserImages(getActivity());
        }
        return searchController;
    }

    //endregion

    //region life cycles

    @Override
    public void onResume()
    {
        super.onResume();
        getActivity().setTitle(R.string.UserPhotos);
        setSearchVisibility(false);
        search(imageController.getOwnerID());
    }

    //endregion
}
